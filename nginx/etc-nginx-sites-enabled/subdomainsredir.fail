#
###
##### service.autoplus.fr
###
#
server {
    listen 80;
    #listen [::]:80;
    server_name service.atp-dev.dtd serviceautoplus.digimondo.net service.autoplus.fr;
    index index.php;
    root /projects/standard.dtd/;
    # PROD
    #error_log /var/log/nginx/vhostsubdomainsredirs.error.log;
    # DEV
    error_log /var/log/nginx/subdomainsredirs.error.log notice;
    access_log /var/log/nginx/subdomainsredirs.access.log;

    set $domain "www.atp-dev.dtd";
    set $baseurl "http://${domain}";

    set $request_url $request_uri;

    location / {

        # http://service.atp-dev.dtd/blabla.js
        if ( $request_uri ~ \.(css|js|ico|png|jpe?g|webp|gif|svg|ttf|ttc|otf|eot|woff|woff2|xml|pdf)$ ){
            return 404;
        }

        # http://service.atp-dev.dtd/category/blabla
        rewrite ^\/tag\/.*$ "${baseurl}/pratique" permanent;
        rewrite ^\/category\/.*$ "${baseurl}/pratique" permanent;

        rewrite ^\/json$ "${baseurl}/pratique" permanent;
        rewrite ^\/crossover$ "${baseurl}/pratique" permanent;

        rewrite ^\/pratique\/$ "${baseurl}/pratique" permanent;
        rewrite ^\/les-dossiers-dauto-plus\/$ "${baseurl}/pratique" permanent;
        rewrite ^\/arnaques-les-dernieres-tendances\/$ "${baseurl}/pratique" permanent;
        rewrite ^\/guide-anti-arnaque-news\/$ "${baseurl}/pratique" permanent;
        rewrite ^\/pv-nos-astuces-nos-conseils\/$ "${baseurl}/pratique" permanent;
        rewrite ^\/departs-en-vacances\/$ "${baseurl}/pratique" permanent;
        rewrite ^\/avant-le-depart-en-vacances\/$ "${baseurl}/pratique" permanent;
        rewrite ^\/comment-mettre-a-jour-votre-gps\/$ "${baseurl}/pratique" permanent;
        rewrite ^\/vos-droits\/$ "${baseurl}/pratique" permanent;
        rewrite ^\/dossiers-assurance-automobile\/$ "${baseurl}/pratique" permanent;
        rewrite ^\/testdm\/$ "${baseurl}/pratique" permanent;
        rewrite ^\/site-under04\/$ "${baseurl}/pratique" permanent;
        rewrite ^\/site-under03\/$ "${baseurl}/pratique" permanent;
        rewrite ^\/site-under02\/$ "${baseurl}/pratique" permanent;
        rewrite ^\/site-under01\/$ "${baseurl}/pratique" permanent;
        rewrite ^\/distributeurs-2017-bis\/$ "${baseurl}/pratique" permanent;
        rewrite ^\/su01\/$ "${baseurl}/pratique" permanent;
        rewrite ^\/su02\/$ "${baseurl}/pratique" permanent;
        rewrite ^\/su03\/$ "${baseurl}/pratique" permanent;
        rewrite ^\/su04\/$ "${baseurl}/pratique" permanent;
        rewrite ^\/su05\/$ "${baseurl}/pratique" permanent;
        rewrite ^\/site-under00-2\/$ "${baseurl}/pratique" permanent;
        rewrite ^\/guide-anti-pv-news\/$ "${baseurl}/pratique" permanent;
        rewrite ^\/distributeurs-2017\/$ "${baseurl}/pratique" permanent;

        rewrite ^\/contacts\/$ "${baseurl}/nous-contacter" permanent;
        rewrite ^\/contacts-web-auto-plus\/$ "${baseurl}/nous-contacter" permanent;
        rewrite ^\/equipe-auto-plus\/$ "${baseurl}/nous-contacter" permanent;
        rewrite ^\/contacts-magazine-et-site-web\/$ "${baseurl}/nous-contacter" permanent;

        # http://service.atp-dev.dtd/garages-confiance/
        rewrite ^\/garages-confiance\/$ "${baseurl}/garage-confiance" permanent;
        rewrite ^\/infos-legales\/$ "${baseurl}/mentions-legales" permanent;
        rewrite ^\/les-archives\/$ "${baseurl}/archives-par-numero" permanent;
        rewrite ^\/charte-des-contributions\/$ "${baseurl}/charte-des-contributions" permanent;
        rewrite ^\/desactiver-adblock-sur-autoplus-fr\/$ "${baseurl}/comment-desactiver-adblock" permanent;
        rewrite ^\/abonnements\/$ "${baseurl}/offre-premium" permanent;

        rewrite ^\/politique-dusage-des-cookies\/$ https://static.digimondo.net/communication/cookies.html permanent;
        rewrite ^\/conditions-generales-dutilisation-des-sites-auto\/$ https://static.digimondo.net/kiosques/CGU_RMM_EMAS.pdf permanent;

        set $args "";

        #
        ## doublons
        # http://service.atp-dev.dtd/guide-des-bonus-malus-2013/
        # /guide-des-bonus-malus-2013/ => /bonus-malus-le-guide/
        rewrite ^\/guide-des-bonus-malus-2013\/$ "${baseurl}/service/r/bonus-malus-le-guide" permanent;
        rewrite ^\/pieges-vehicule-occasion\/$ "${baseurl}/service/r/occasion-pas-chere-les-pieges-a-eviter" permanent;
        rewrite ^\/50-conseils-occasion\/$ "${baseurl}/service/r/50-conseils-pour-acheter-une-voiture-doccasion" permanent;
        rewrite ^\/les-arnaques-des-petites-annonces\/$ "${baseurl}/service/r/acheter-une-voiture-doccasion-eviter-les-arnaques" permanent;
        ## /fin des doublons
        #

        # http://service.atp-dev.dtd/testblabla
        #if ( $request_uri ~ ^\/(.+)$ ){
        if ( $request_uri ~ ^\/(.*)\/?$ ){
            set $args1 $1;
            #rewrite ^ "${baseurl}/service/r/${args1}" permanent;
            set $request_url "/service/r/${args1}";
            rewrite '^(.*)$' '/index.php' last;
        }
    }

    location ~ \.php$ {

        fastcgi_pass php-upstream;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;

        fastcgi_buffer_size 128k;
        fastcgi_buffers 4 256k;
        fastcgi_busy_buffers_size 256k;

        fastcgi_param  PATH_INFO          $fastcgi_path_info;
        fastcgi_param  PATH_TRANSLATED    $document_root$fastcgi_path_info;
        fastcgi_param  SCRIPT_NAME        $fastcgi_script_name;
        fastcgi_param  SCRIPT_FILENAME    $document_root$fastcgi_script_name;
        fastcgi_param  REQUEST_URI        $request_url;
        fastcgi_index index.php;
    }
}