#!/bin/bash

# /projects/docker-ezlight/php-fpm/start.sh

set -x

# pour les scripts header biding dans /projects/reworld_hb
mkdir /root/.ssh
chmod 700 /root/.ssh 
cp -f /projects/xavier-ssh-dir/id_rsa /root/.ssh/id_rsa
chmod 600 /root/.ssh/id_rsa

begindir="$( pwd )"

wwwdirname=standard.dtd
wwwdir="/projects/${wwwdirname}"
projectdirname="repo-${wwwdirname}"
projectdir="/projects/${projectdirname}"

kernelbranch=master
branch=dev
env=dev
gituser=MyGitUser
gitpasswd=MyGitPassword!
# si il y a des caracteres à echapper dans le mot de pass comme par exemple un point d'exlamation. le faire ici :
gitpasswdsed="MyGitPassword\!"

echo -e "Configuration du script : \n"
echo -e "Repertoire Projets ${projectdirname} : ${projectdir}"
echo -e "Repertoire WWW ${wwwdirname} : ${wwwdir}"
echo -e "Branche Kernel : $kernelbranch"
echo -e "Branch du standard : $branch"
echo -e "Environnement : $env"
echo -e "Utilisateur GIT : $gituser"
#echo -e "Password GIT : $gitpasswd \n"

#exit 1;

source /projects/docker-ezlight/php-fpm/list-ez-sites.sh


##
# Check repertoires
##

########################### pendant les tests #####
# rm -rf $projectdir
# rm -rf $wwwdir
###################################################

if [ ! -d $projectdir ]; then
    mkdir -p $projectdir
fi

if [ ! -d $wwwdir ]; then
    mkdir -p $wwwdir
fi


##
# Installation sources projets
##

if [ -n "$(find $projectdir -maxdepth 0 -empty)" ] || [ -n "$(find $wwwdir -maxdepth 0 -empty)" ];
then

    # on recommence tout
    rm -rf $projectdir
    rm -rf $wwwdir
    mkdir -p $wwwdir
    mkdir -p $projectdir

    source /projects/docker-ezlight/php-fpm/setup-project-n-www.sh

else
      echo "Directory \"${projectdir}\" > contains files > Setup already"
fi




##
# Cache + autoload
##
echo -e "\nVidage de cache ..."
cd $wwwdir
rm -rf var/cache/*
echo -e "\nAutoload ..."
#su -c "php bin/php/ezpgenerateautoloads.php" www-data
#su -c "php bin/php/ezpgenerateautoloads.php -o" www-data
php bin/php/ezpgenerateautoloads.php
php bin/php/ezpgenerateautoloads.php -o


##
# Fichiers Root
##
echo -e "\nFichiers Root ..."
cd $projectdir
find "$projectdir/application/root/" -name "*.${env}" -type f -exec cp -f {} $wwwdir \;
echo -e "\nRename files without env extension"
cd $wwwdir
find ./ -maxdepth 1 -name "*.${env}" -type f | sed -e "p;s/.${env}$//i" | xargs -n2 mv -f


##
# Ajout des crontab
##
# echo -e "\nCrontabs ..."
# cd $projectdir
# find "$wwwdir/" -maxdepth 1 -name "*.crontab" -exec ln -sf {} /etc/cron.d/ \;
# chown root:root "$wwwdir/*crontab" > /dev/null 2>&1;
# chmod 0644 "$wwwdir/*crontab" > /dev/null 2>&1;
# chown root:root "/etc/cron.d/*crontab" > /dev/null 2>&1;
# chmod 0644 "/etc/cron.d/*crontab" > /dev/null 2>&1;


##
# Config PHP-FPM
##
# echo -e "\nConfiguration basique PHP-FPM ..."
# cd $begindir
# fpmpoolconf="/etc/php/7.3/fpm/pool.d/www.conf";
# mkdir -p /run/php # depuis que g mis php 7.3
# chmod 777 /run/php
# cp -f '/projects/mf-xavier-langlois/docker/ezlight/lnmp/files/php-fpm/www.conf' $fpmpoolconf;
#
# # PHP.ini affichage des erreurs
# sed -i 's/display_errors = Off/display_errors = On/' /etc/php/7.3/fpm/php.ini
# sed -i 's/display_startup_errors = Off/display_startup_errors = On/' /etc/php/7.3/fpm/php.ini


##
# Config NGINX
##
# echo -e "\nConfiguration basique NGINX ..."
# cd $begindir
# nginxconf="/etc/nginx/nginx.conf";
# cp -f '/projects/mf-xavier-langlois/docker/ezlight/lnmp/files/nginx/nginx.conf' $nginxconf;


##
# Virtualhost
##
echo -e "\nCreation des VHOSTS NGINX ..."
cd $begindir
for siteaccess in ${!sites[*]}
do
    project=${siteaccess}
    hostname=${sites[$siteaccess]}
    prodHost=${sitesProd[$siteaccess]}
    domain="standard.dtd"
    vhostenabledfilename="/projects/docker-ezlight/nginx/etc-nginx-sites-enabled/${hostname}.conf"

    #Copy vhost to sites-avaiable
    if [[ -e $vhostenabledfilename ]]; then
       rm $vhostenabledfilename;
    fi
    # selfdir="$(dirname "${BASH_SOURCE[0]}")"
    if [ -e "/projects/docker-ezlight/php-fpm/vhost.template.nginx.conf" ]; then
        cp -f '/projects/docker-ezlight/php-fpm/vhost.template.nginx.conf' $vhostenabledfilename;

        if [ -e "$vhostenabledfilename" ]; then
            #Change variable
            sed -i 's/\${SiteHostName}/'$hostname'/g' $vhostenabledfilename
            sed -i 's/\${SiteWWWName}/'$domain'/g' $vhostenabledfilename
            sed -i 's/\${ProdHostName}/'$prodHost'/g' $vhostenabledfilename
            chmod 777 $vhostenabledfilename

        else
            echo -e "\nCopie du Template de VHOST NGinx echouee"
        fi
    else
        echo -e "\nTemplate de VHOST NGinx manquant"
    fi
done


#### special pour les appels curl de tls à gtv ####
# on rajoute l'ip de ezlight-nginx
# docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ezlight-nginx
NGINXIP="$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ezlight-nginx)"
echo "${NGINXIP} www.tls-dev.dtd admin.tls-dev.dtd" >> /etc/hosts
echo "${NGINXIP} www.gtv-dev.dtd admin.gtv-dev.dtd" >> /etc/hosts
echo "${NGINXIP} www.cs-dev.dtd admin.cs-dev.dtd" >> /etc/hosts
echo "${NGINXIP} www.sev-dev.dtd admin.sev-dev.dtd" >> /etc/hosts
echo "${NGINXIP} www.geh-dev.dtd admin.geh-dev.dtd" >> /etc/hosts
echo "${NGINXIP} www.jsv-dev.dtd admin.jsv-dev.dtd" >> /etc/hosts
echo "${NGINXIP} www.pv-dev.dtd admin.pv-dev.dtd" >> /etc/hosts
echo "${NGINXIP} www.met-dev.dtd admin.met-dev.dtd" >> /etc/hosts
echo "${NGINXIP} www.gz-dev.dtd admin.gz-dev.dtd" >> /etc/hosts
echo "${NGINXIP} www.ts-dev.dtd admin.ts-dev.dtd" >> /etc/hosts
echo "${NGINXIP} www.dps-dev.dtd admin.dps-dev.dtd" >> /etc/hosts
echo "${NGINXIP} www.rp-dev.dtd admin.rp-dev.dtd" >> /etc/hosts
echo "${NGINXIP} www.ea-dev.dtd admin.ea-dev.dtd" >> /etc/hosts
echo "${NGINXIP} www.atp-dev.dtd admin.atp-dev.dtd" >> /etc/hosts

#### ADMINER ####
# echo -e "\nActivation Virtualhost Nginx pour ADMINER ..."
# # Adminer : sources
# mkdir -p /var/www
# rm -rf /var/www/adminer
# ln -fs /projects/mf-xavier-langlois/docker/ezlight/lnmp/files/adminer /var/www/adminer;
# chmod -R 775 /var/www/adminer
# # Adminer : nginx
# vhostavailablefilename='/etc/nginx/sites-available/999-adminer'
# vhostenabledfilename='/etc/nginx/sites-enabled/999-adminer'
# if [[ -e $vhostavailablefilename ]]; then
#    rm $vhostavailablefilename;
# fi
# cp -f '/projects/mf-xavier-langlois/docker/ezlight/lnmp/files/nginx/adminer.conf' $vhostavailablefilename;
# ln -sf $vhostavailablefilename $vhostenabledfilename;


##
# LAUNCH NGINX
##


# with docker client
docker exec ezlight-nginx nginx -s reload



##
# SITES EN SYMFONY
##

source /projects/docker-ezlight/php-fpm/list-symfony-sites.sh
for project in ${!sfsites[*]}
do
    #server_name=${sites[$project]}
    if [ -d "/projects/$project" ]; then
        #
        ### ajout des crons ... ne fonctionne pas sur alpine
        #
        # if [ -f /projects/$project/files-php/crontab.cron ]; then
        #     cat /projects/$project/files-php/crontab.cron > /var/spool/cron/crontabs/root
        # fi
        ### Recours au système DMG ... le système D de McGyver
        if [ -f /projects/$project/files-php/simulate-cron.sh ]; then
            chmod o+x /projects/$project/files-php/simulate-cron.sh
            nohup /projects/$project/files-php/simulate-cron.sh &
        fi
        #
        ### compilation des less en css
        #
        cd /projects/$project/symfony
        yarn install
        # en dev
        nohup yarn encore dev --watch &
        # prod  ?
        # nohup yarn encore prod --watch &
    fi
done


##
# LAUNCH PHP IF NEEDED
##

if pgrep -x "php-fpm7" > /dev/null
then
    echo "php-fpm7 (cf : cat /proc/PID/comm ) already running"
else
    php-fpm7 -F
fi
