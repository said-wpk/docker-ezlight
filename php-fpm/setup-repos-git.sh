#!/bin/bash

set -x

begindir="$( pwd )"

wwwdirname=standard.dtd
wwwdir="/projects/${wwwdirname}"
projectdirname="repo-${wwwdirname}"
projectdir="/projects/${projectdirname}"

kernelbranch=master
branch=dev
env=dev

source /projects/docker-ezlight/php-fpm/list-ez-sites.sh

mkdir -p $wwwdir
mkdir -p $projectdir

chmod -R 777 $wwwdir
chmod -R 777 $projectdir

##
# Installation sources projets
##

echo -e "\nInstallation source projet mfstandard ..."
cd "$projectdir/.."
git clone -b$branch git@bitbucket.org:dtdmondadori/standard.digimondo.net.git $projectdirname
wait

##
# Git ignore permission
##
echo -e "\n$projectdir Git ignore permission ..."
cd $projectdir
git config core.fileMode false

##
# Changement des chemins des sous modules pour user / pass
##
echo -e "\nChangement des chemins des sous modules pour user / pass ..."
cd $projectdir

##
# Checkout des extensions sur la branche dev
##
echo -e "\nCheckout git des extensions sur la branche dev ..."
cd $projectdir
git checkout $branch
git fetch origin
git pull --rebase origin $branch
git submodule init
git submodule update
git submodule foreach git checkout $branch
git submodule foreach git pull --rebase origin $branch
git submodule foreach git config core.fileMode false


##
# Installation sources www
##

echo -e "\nInstallation source eZPublish 5.x Legacy ..."

cd $wwwdir
git init
git remote add origin git@bitbucket.org:dtdmondadori/kernel-ezpublish-legacy.git


wait
git fetch origin
git pull origin master



git fetch origin
git checkout $branch
git pull --rebase origin $branch

##
# Git ignore permission
##
echo -e "\nGit ignore permission ..."
cd $wwwdir
git config core.fileMode false

##
# Fichiers env.php
##
echo -e "\nCreation env.php ..."
cd $wwwdir
echo $env > env.php

##
# On change le proprietaire du documentRoot du projet
##
echo -e "\nChangement proprietaire et droits du projet ..."
cd $wwwdir
chmod 755 -R .
chown www-data:www-data -R .
if [ ! -d "var" ]; then
    mkdir -p "var"
fi
chmod 775 -R var/
chown www-data:www-data -R var/

##
# Liens symboliques vers extensions et settins et droits
##
echo -e "\nCreation des liens symboliques ..."
cd $wwwdir
#rm -f extension
if [ ! -e "extension" ]; then
    ln -sf "$projectdir/application/extension" extension
fi
projectsymlink="$projectdir/application/extension"
if [ -e $projectsymlink ]; then
	while [[ $projectsymlink != "/" ]]; do echo "Mettre en executable "$projectsymlink;chmod +x $projectsymlink; projectsymlink=$(dirname $projectsymlink); done;
fi

#rm -f settings/override
if [ ! -e "settings/override" ]; then
    ln -sf "$projectdir/application/settings/override" settings/override
fi
projectsymlink="$projectdir/application/settings/override"
if [ -e $projectsymlink ]; then
	while [[ $projectsymlink != "/" ]]; do echo "Mettre en executable "$projectsymlink;chmod +x $projectsymlink; projectsymlink=$(dirname $projectsymlink); done;
fi

#rm -f settings/default
if [ ! -e "settings/default" ]; then
    ln -sf "$projectdir/application/settings/default" settings/default
fi
projectsymlink="$projectdir/application/settings/default"
if [ -e $projectsymlink ]; then
	while [[ $projectsymlink != "/" ]]; do echo "Mettre en executable "$projectsymlink;chmod +x $projectsymlink; projectsymlink=$(dirname $projectsymlink); done;
fi

chmod 755 -R extension
chown www-data:www-data -R extension
chmod 755 -R settings/override
chown www-data:www-data -R settings/override
chmod 755 -R settings/default
chown www-data:www-data -R settings/default

##
# Cache DFS
##

cd $wwwdir
if [ ! -d var/DFSVar ]; then
    mkdir -p var/DFSVar
    chmod 775 -R var/DFSVar
    chown www-data:www-data -R var/DFSVar
fi

for siteaccess in ${!sites[*]}
do
    project=${siteaccess}
    hostname=${sites[$siteaccess]}

    if [ ! -d var/DFSVar/var/$project/cache ]; then
        echo "Creation du repertoire var/DFSVar/var/$project/cache"
        mkdir -p var/DFSVar/var/$project/cache
        chmod 775 -R var/DFSVar/var/$project/cache
        chown www-data:www-data -R var/DFSVar/var/$project/cache
    fi

    if [ ! -d var/DFSVar/var/$project/storage ]; then
        echo "Creation du repertoire var/DFSVar/var/$project/storage"
        mkdir -p var/DFSVar/var/$project/storage
        chmod 775 -R var/DFSVar/var/$project/storage
        chown www-data:www-data -R var/DFSVar/var/$project/storage
    fi
done
